$(document).ready(function () {
    $(".fancybox-button").fancybox({
        openEffect: 'fade',
        closeEffect: 'fade',
        closeClick: true,
        scrolling: false,
        arrows: true,

        helpers: {
            title: {
                type: 'inside'
            },
            overlay: {
                showEarly: false
            },
            buttons: {}
        }
    });
});

$(document).ready(function () {
    $(".popconfirm").popConfirm();
});

$(document).ready(function () {
    $("#cols1").on('click', function () {
        $(".panel-collapse").collapse('show');
    });
});

$(document).ready(function () {
    $("#cols2").on('click', function () {
        $(".panel-collapse").collapse('hide');
    });
});

$(document).ready(function () {
    $("#cols1").on('click', function () {
        $(".panel-collapse").collapse('show');
    });
});