<!DOCTYPE HTML>
<html>
<head>
    <title>Advanced Run</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link type="image/x-icon" href="/static/favicon.ico" rel="icon">
    <script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/jquery.numeric.min.js"></script>
    <link rel="stylesheet" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/css/own.css"/>
    <script>
        $(document).ready(function () {
            jQuery('.td-input').val('');
            jQuery('#threads').val(1);
            $(".numeric").numeric();
        });
        function selectAll() {
            var i = 1;
            jQuery('.td-input').val(function () {
                return i++;
            });
        }
        function deselectAll() {
            jQuery('.td-input').val('');
        }

        $(document).ready(function () {
            $("#modalButton").click(function () {
                $.ajax({
                    url: "preview/adv",
                    type: "POST",
                    data: JSON.stringify(jQuery('#formid').serializeArray()),
                    dataType: "text",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        $("#myModal").modal("toggle");
                        $("#xml").html(data);
                    }
                });
            });
        });
    </script>
</head>
<body>
<#include "menu.ftl">
<div class="container">
    <div class="row">
        <form action="/${project}/build/adv" method="post" id="formid" autocomplete="off">
            <div class="row top-row">
                <div class="col-md-3">
                    <input type="submit" class="btn btn-info" value="Generate XML">
                    <button type="button" class="btn btn-success" id="modalButton">Preview XML</button>
                </div>
                <div class="col-md-3 col-md-offset-6">
                    <button type="button" class="btn btn-info" onclick="selectAll()">Select All</button>
                    <button type="button" class="btn btn-info" onclick="deselectAll()">Deselect All</button>
                </div>
            </div>
            <div class="row">
                <p class="alert alert-info">
                    Use
                    <input id="threads" type="number" name="threads" style="width: 50px;margin: 0px 4px;"
                           value="1">
                    threads
                </p>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Groups</th>
                    <th>Test №</th>
                </tr>
                </thead>
                <tbody>
                <#list groupsRun as group>
                <tr>
                    <td>${group}</td>
                    <td class="td-row">
                        <input type="text" name="test" class="td-input numeric" placeholder="Test №">
                    </td>
                </tr>
                </#list>
                </tbody>
            </table>
        </form>
    </div>
</div>
<#include "modal.ftl">
</body>
</html>







