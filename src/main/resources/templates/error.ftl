<!DOCTYPE HTML>
<html>
<head>
    <title>Error</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link type="image/x-icon" href="/static/favicon.ico" rel="icon">
    <script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/css/bootstrap.min.css"/>
</head>
<body>
<#include "menu.ftl">
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div align="center" class="alert alert-danger"><p style="font-size: 16px">${error}</p></div>
        </div>
    </div>
</div>
</body>
</html>







