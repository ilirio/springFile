<!DOCTYPE HTML>
<html>
<head>
    <title>${project}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link type="image/x-icon" href="/static/favicon.ico" rel="icon">
    <script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/jquery.mousewheel-3.0.6.pack.js"></script>
    <script type="text/javascript" src="/js/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript" src="/js/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <script type="text/javascript" src="/js/jquery.popconfirm.js"></script>
    <script type="text/javascript" src="/js/own.js"></script>
    <link rel="stylesheet" href="/css/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/css/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen"/>
    <link rel="stylesheet" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/css/own.css"/>
</head>
<body>
<#include "menu.ftl">
<div class="container">
    <div class="row">
        <div class="col-md-2 col-md-offset-10">
            <button class="btn btn-info" id="cols1">Expand</button>
            <button class="btn btn-info" id="cols2">Collapse</button>
        </div>
    </div>
<#if listTemplates?size == 0>
    <div align="center" class="alert alert-danger" style="margin-top: 10px">
        <p style="font-size: 16px">There are no screens in project folder !</p>
    </div>
<#else>
<#list listTemplates as template>
<div class="panel-group">
	<#if listScreens?seq_contains(template + "_ComparedImage_.png") ||
	listScreens?seq_contains(template + "_DiffImage_.png") ||
	listScreens?seq_contains(template + "_DiffImageTransparent_.png")>
		<#assign diff="true">
    <div class="panel panel-danger">
	<#else>
    <div class="panel panel-info">
	</#if>
    <div class="panel-heading">
        <h4 class="panel-title">
            <a class="btn-block" data-toggle="collapse" href="#${template?lower_case}">
			${template}
            </a>
        </h4>
    </div>
    <div id="${template?lower_case}" class="panel-collapse collapse">
        <div class="panel-body">
            <div class="row">
                <form action="/${project}/delete" method="post">
                    <div class="col-md-10 col-md-offset-1">
						<#list listScreens as screen>
							<#if screen?contains(template)>
                                <div class="col-md-6 inner">
                                    <p>${screen}</p>
                                    <a class="fancybox-button" rel="fancybox-button"
                                       href="${project}/image/${screen}">
                                        <img class="img-thumbnail" style="width:350px;height:250px;"
                                             src="${project}/image/${screen}"/>
                                    </a>

                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default">
                                            <input type="checkbox" name="screenUtils" value="${screen}">
                                            <span class="glyphicon glyphicon-ok"></span>
                                        </label>
                                    </div>
                                </div>
							<#else>
							</#if>
						</#list>
                    </div>
                    <div class="col-md-2 col-md-offset-4">
                        <input type="submit" class="btn btn-info popconfirm" value="Delete">
                    </div>
                </form>
				<#if diff?? && diff?matches("true")>
					<#assign diff="">
                    <div class="col-md-1">
                        <form action="/${project}/make/${template}" method="post">
                            <input type="submit" class="btn btn-info popconfirm" value="Make Baseline"/>
                        </form>
                    </div>
				</#if>
            </div>
        </div>
    </div>
</div>
</#list>
</#if>
</div>
</body>
</html>