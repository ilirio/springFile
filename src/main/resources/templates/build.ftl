<!DOCTYPE HTML>
<html>
<head>
    <title>RUN</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link type="image/x-icon" href="/static/favicon.ico" rel="icon">
    <script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/own.js"></script>
    <link rel="stylesheet" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/css/own.css"/>
</head>
<body>
<#include "menu.ftl">
<div class="container">
    <form action="/${project}/run" method="post">
        <div class="row">
            <div class="row">
                <div class="col-md-2  alert">
                    <input type="submit" class="btn btn-info" value="Generate XML">
                </div>
            </div>
            <div class="row" style="position:fixed">
                <p class="alert alert-info">
                    Use
                    <input id="example-large-options" type="number" name="threads" style="width: 50px;margin: 0px 4px;" value="1">
                    threads
                </p>
            </div>
        <#list groupsRun as group>
            <div class="row">
                <div class="col-md-11">
                    <ul class="list-group">
                        <li class="list-group-item list-group-item-info">${group}</li>
                    </ul>
                </div>
                <div class="col-md-1">
                        <input maxlength="3" type="number" name="test" class="form-control" placeholder="Test №">
                </div>
            </div>
        </#list>
        </div>
    </form>
</div>
</body>
</html>







