<!DOCTYPE HTML>
<html>
<head>
    <title>Projects</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link type="image/x-icon" href="/static/favicon.ico" rel="icon">
    <link rel="stylesheet" href="/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="/css/own.css"/>
</head>
<body>
<#include "menu.ftl">
<div class="container">
    <table class="table table-striped">
        <thead>
        <tr>
	        <th>№</th>
            <th>Jenkins Link</th>
            <th>Screens Page</th>
            <th>Project Build</th>
        </tr>
        </thead>
        <tbody>
        <#list folders as folder>
        <tr>
            <td>${folders?seq_index_of(folder) + 1}</td>
            <td><a href="${jenkinsUrl}/${folder}">${folder}</a></td>
            <td><a href="/${folder}">Screens</a></td>
            <td><a href="/${folder}/build/simple">Simple</a><i> / </i><a href="/${folder}/build/adv">Advanced</a></td>
        </tr>
        </#list>
        </tbody>
    </table>
</div>
</body>
</html>


