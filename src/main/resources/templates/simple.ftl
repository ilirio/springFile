<!DOCTYPE HTML>
<html>
<head>
    <title>Simple Run</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link type="image/x-icon" href="/static/favicon.ico" rel="icon">
    <script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap-toggle.min.js"></script>
    <link rel="stylesheet" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/css/bootstrap-toggle.min.css">
    <link rel="stylesheet" href="/css/own.css"/>
    <script>
        $(document).ready(function () {
            jQuery("input[name='test']").bootstrapToggle('off');
        });
        function allOff() {
            jQuery("input[name='test']").bootstrapToggle('off');
        }
        function allOn() {
            jQuery("input[name='test']").bootstrapToggle('on');
        }

        $(document).ready(function () {
            $("#modalButton").click(function () {
                $.ajax({
                    url: "preview/simple",
                    type: "POST",
                    data: JSON.stringify(jQuery('#formid').serializeArray()),
                    dataType: "text",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        $("#myModal").modal("toggle");
                        $("#xml").html(data);
                    }
                });
            });
        });
    </script>
</head>
<body>
<#include "menu.ftl">
<div class="container">
    <div class="row">
        <form action="/${project}/build/simple" method="post" id="formid">
            <div class="row top-row">
                <div class="col-md-3">
                    <button type="submit" class="btn btn-info" id="cols1">Generate XML</button>
                    <button type="button" class="btn btn-success" id="modalButton">Preview XML</button>
                </div>
                <div class="col-md-2 col-md-offset-7">
                    <button type="button" class="btn btn-info" onclick="allOn()">All On</button>
                    <button type="button" class="btn btn-info" onclick="allOff()">All Off</button>
                </div>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Группа/Тест</th>
                    <th>Отметить</th>
                </tr>
                </thead>
                <tbody>
                <#list groupsRun as group>
                    <tr>
                        <td>${group}</td>
                        <td>
                            <input id="toggle-trigger" data-onstyle="info" maxlength="3" data-toggle="toggle"
                                   type="checkbox" name="test" value="${group?index}" placeholder="Test №">
                        </td>
                    </tr>
                </#list>
                </tbody>
            </table>
        </form>
    </div>
</div>
<#include "modal.ftl">
</body>
</html>







