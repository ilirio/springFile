package springfreemarker.authcore;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.GeneralSecurityException;
import java.util.Timer;

public class CoreAuth
{
	Timer timer;

//	public void reminder(String secret) {
//		timer = new Timer();
//		timer.scheduleAtFixedRate(new TimedPin(secret), 0, 1 * 1000);
//	}

//	public static void main(String[] args)
//	{
//		CoreAuth.computePin("qweqeqwe");
//	}
//	int count=1;
//	class TimedPin extends TimerTask
//	{
//		private String secret;
//		public TimedPin (String secret){
//			this.secret=secret;
//		}
//		String previouscode="";
//		public void run() {
//			String newout = CoreAuth.computePin(secret);
//			if(previouscode.equals(newout)){
//				System.out.print(".");
//			}
//			else {
//				if(count<=30){
//					for (int i=count+1; i<=30;i++){
//						System.out.print("+");
//					}
//				}
//				System.out.println(": "+ newout + " :");
//				count=0;
//			}
//			previouscode = newout;
//			count++;
//		}
//	}

	public static String computePin(String secret)
	{
		if(secret == null || secret.length() == 0)
		{
			return "Null or empty secret";
		}
		try
		{
			final byte[] keyBytes = Base32String.decode(secret);
			Mac mac = Mac.getInstance("HMACSHA1");
			mac.init(new SecretKeySpec(keyBytes, ""));
			PasscodeGenerator pcg = new PasscodeGenerator(mac);
			return pcg.generateTimeoutCode();
		}
		catch(GeneralSecurityException e)
		{
			return "General security exception";
		}
		catch(Base32String.DecodingException e)
		{
			return "Decoding exception";
		}
	}
}
