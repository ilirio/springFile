package springfreemarker.entities;

public class TotpCode
{
	String code;

	public TotpCode(String code)
	{
		this.code = code;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}
}
