package springfreemarker.utils;


import org.codehaus.jackson.map.ObjectMapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

public class SerializationMethods
{
	public static String toXML(Object object)
	{
		StringWriter data = null;
		try
		{
			data = new StringWriter();
			JAXBContext context = JAXBContext.newInstance(object.getClass());
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(object, data);
		}
		catch (JAXBException e)
		{
			e.printStackTrace();
		}
		return data.toString();
	}

	public static Object toObjectFromXML(String data, Class clazz)
	{
		try
		{
			JAXBContext context = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			return unmarshaller.unmarshal(new StringReader(data));
		}
		catch (JAXBException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public static Object toObjectFromJson(String data, Class clazz)
	{
		try
		{
			return new ObjectMapper().readValue(data, clazz);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public static String toJson(Object object)
	{
		try
		{
			ObjectMapper objectMapper = new ObjectMapper();
			return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public static <T> List<T> toObjectFromJsonList(String data, Class<T> clazz)
	{
		try
		{
			ObjectMapper objectMapper = new ObjectMapper();
			return objectMapper.readValue(data, objectMapper.getTypeFactory().constructCollectionType(List.class, clazz));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
