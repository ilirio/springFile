package springfreemarker.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@PropertySource("classpath:config.properties")
@PropertySource(value = "file:${prop.file}", ignoreResourceNotFound = true)
@Component
public class ScreensData
{
	private static final Logger logger = LoggerFactory.getLogger(ScreensData.class);

	@Autowired
	Path path;

	@Value("${placeholder}")
	private String placeholderPath;

	public byte[] getScreenData(String project, String imageId)
	{
		try
		{
			return Files.readAllBytes(new File(path.getScreensFolderPathWithProject(project) + imageId).toPath());
		}
		catch(IOException e)
		{
			logger.error("Error!", e);
		}
		return getPlaceholder();
	}

	public byte[] getPlaceholder()
	{
		try
		{
			return Files.readAllBytes(new File(placeholderPath).toPath());
		}
		catch(IOException e)
		{
			logger.error("Error!", e);
		}
		return null;
	}

}
