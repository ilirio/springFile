package springfreemarker.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

@PropertySource("classpath:config.properties")
@PropertySource(value = "file:${prop.file}", ignoreResourceNotFound = true)
@Component
public class Path
{
	private static final Logger logger = LoggerFactory.getLogger(Path.class);

	@Value("${base.path}")
	private String baseFolder;

	@Value("${screen.path}")
	private String screensPath;

	@Value("${xml.path}")
	private String xmlPath;

	@Value("${jenkins.url}")
	private String jenkinsUrl;

	public String getBaseFolder()
	{
		return baseFolder;
	}

	public void setBaseFolder(String baseFolder)
	{
		this.baseFolder = baseFolder;
	}

	public String getScreensPath()
	{
		return screensPath;
	}

	public void setScreensPath(String screensPath)
	{
		this.screensPath = screensPath;
	}

	public String getScreensFolderPathWithProject(String project)
	{
		return getProjectPath(project) + screensPath;
	}

	public String getProjectPath(String project)
	{
		return baseFolder + project;
	}

	public ArrayList<String> getProjectFolderList()
	{
		File folder = new File(getBaseFolder());
		File[] files = folder.listFiles();
		ArrayList<String> collect = new ArrayList<>();
		if(files != null)
		{
			collect = Arrays.stream(files).map(File::getName).collect(Collectors.toCollection(ArrayList::new));
		}
		logger.debug("ProjectFolderList {}", collect);
		return collect;
	}

	public String getXmlPathWithProject(String project)
	{
		return getProjectPath(project) + "/" + xmlPath;
	}

	public String getJenkinsUrlWithProject(String project)
	{
		return jenkinsUrl + project;
	}

	public String getXmlPath()
	{
		return xmlPath;
	}

	public String getJenkinsUrl()
	{
		return jenkinsUrl;
	}
}
