package springfreemarker.utils;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

@Component
public class HttpUtils
{
	public static final String JSON = "application/json";
	public static final String XML = "text/xml";

	private ArrayList<BasicHeader> headers;

	private String url;
	private String message;

	public HttpUtils()
	{
		headers = new ArrayList<BasicHeader>();
	}

	public HttpUtils(String url)
	{
		this.url = url;
		headers = new ArrayList<BasicHeader>();
	}

	public HttpUtils(URI uri)
	{
		this.url = uri.toString();
		headers = new ArrayList<BasicHeader>();
	}

	public HttpUtils setMessage(String message)
	{
		this.message = message;
		return this;
	}

	public HttpUtils setUrl(String url)
	{
		this.url = url;
		return this;
	}

	public HttpUtils addHeader(String name, String value)
	{
		headers.add(new BasicHeader(name, value));
		return this;
	}

	public String sendPostRequest()
	{
		String data = "Error!";
		HttpPost httpPost = new HttpPost(url);
		if (!headers.isEmpty())
		{
			for (BasicHeader header : headers)
			{
				httpPost.addHeader(header);
			}
		}
		StringEntity myEntity = new StringEntity(message, ContentType.create("UTF-8"));
		httpPost.setEntity(myEntity);
		try
		{
			CloseableHttpResponse response = HttpClients.createDefault().execute(httpPost);
			HttpEntity entity = response.getEntity();
			data = EntityUtils.toString(entity, "UTF-8");
			EntityUtils.consume(entity);
		}
		catch (IOException e)
		{
			System.out.println("Не удалось выполнить запрос " + e.getMessage());
			e.printStackTrace();
		}
		return data;
	}

	public String sendGetRequest() throws IOException
	{
		HttpGet httpGet = new HttpGet(url);
		if (!headers.isEmpty())
		{
			for (BasicHeader header : headers)
			{
				httpGet.addHeader(header);
			}
		}
		CloseableHttpResponse response = HttpClients.createDefault().execute(httpGet);
		HttpEntity entity = response.getEntity();
		String data = EntityUtils.toString(entity, "UTF-8");
		EntityUtils.consume(entity);
		return data;
	}
}
