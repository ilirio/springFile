package springfreemarker.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ScreenUtils
{
	private static final Logger logger = LoggerFactory.getLogger(ScreenUtils.class);

	@Autowired
	Path path;

	public List<String> getScreensList(String project)
	{
		LinkedList<String> listScreens = new LinkedList<>();
		File screenFolder = new File(path.getScreensFolderPathWithProject(project));
		File[] files = screenFolder.listFiles();
		if(files != null)
		{
			listScreens = Arrays.stream(files)
					.filter(file -> file.getName().endsWith(".png") || file.getName().endsWith(".jpg"))
					.map(File::getName)
					.collect(Collectors.toCollection(LinkedList::new));
		}
		logger.debug("listScreens {}", listScreens);
		return listScreens;
	}

	public List<String> getListTemplates(String project)
	{
		List<String> listTemplates = new LinkedList<>();
		File screenFolder = new File(path.getScreensFolderPathWithProject(project));
		File[] files = screenFolder.listFiles();
		HashSet<String> collect;
		if(files != null)
		{
			collect = Arrays.stream(files)
					.filter(file -> file.getName().endsWith(".png") || file.getName().endsWith(".jpg"))
					.map(file -> file.getName().substring(0, file.getName().indexOf("_")))
					.collect(Collectors.toCollection(HashSet::new));
		}else return listTemplates;
		listTemplates.addAll(collect);
		logger.debug("listTemplates {}", listTemplates);
		return listTemplates;
	}

	public void deleteScreens(String[] screensNames, String project)
	{
		File dir = new File(path.getScreensFolderPathWithProject(project));
		File[] files = dir.listFiles();
		if(files != null)
		{
			Arrays.stream(files)
					.filter(file -> Arrays.asList(screensNames).contains(file.getName()))
					.forEach(File::delete);
		}
	}

	public void makeImageBase(String project, String template)
	{
		File baseFile = null;
		File diffFile = null;
		File folder = new File(path.getScreensFolderPathWithProject(project));
		File[] files = folder.listFiles();
		if(files != null)
		{
			for(File screen : files)
			{
				if(screen.getName().contains(template + "_ComparedImage_"))
				{
					diffFile = screen;
					logger.debug("diffFile {}", diffFile);
				} else if(screen.getName().contains(template + "_Base"))
				{
					baseFile = screen;
					logger.debug("baseFile {}", baseFile);
				} else if(screen.getName().contains(template))
				{
					boolean delete = screen.delete();
					logger.debug("file was deleted {}", delete);
				}
			}
		}
		try
		{
			boolean delete = baseFile.delete();
			boolean renameTo = diffFile.renameTo(baseFile);
			logger.debug("File {}, was deleted {}, and File {} was renamed {}", baseFile, delete, diffFile, renameTo);
		}
		catch(NullPointerException e)
		{
			logger.error("Error!", e);
		}
	}
}