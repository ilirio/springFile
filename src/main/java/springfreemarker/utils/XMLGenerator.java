package springfreemarker.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import springfreemarker.entities.FormJson;
import springfreemarker.entities.Groups;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;


@Component
public class XMLGenerator
{
	private static final Logger logger = LoggerFactory.getLogger(XMLGenerator.class);

	@Value("${groups.filename}")
	private String groupsFileName;

	@Autowired
	ScreenUtils screenUtils;
	@Autowired
	Path path;

	public String generateSimpleSuite(ArrayList<String> testsNames)
	{
		XmlSuite.DEFAULT_THREAD_COUNT = 0;
		XmlSuite.DEFAULT_VERBOSE = 0;
		XmlSuite suite = new XmlSuite();
		suite.setName("TestSuite");
		suite.setVerbose(1);
		XmlTest test = new XmlTest(suite);
		test.setName("CustomTest");
		List<XmlClass> classes = new ArrayList<XmlClass>();
		List<String> groups = new ArrayList<String>();
		groups.addAll(testsNames);
		classes.add(new XmlClass("pb.autotests.TestRunner", false));
		test.setXmlClasses(classes);
		test.setIncludedGroups(groups);
		logger.debug("Generated XML {}", suite.toString());
		return suite.toXml();
	}

	public String generateMultipleTestSuite(Map<Integer, List<String>> testsNames, int threads, String project)
	{
		XmlSuite.DEFAULT_THREAD_COUNT = 0;
		XmlSuite.DEFAULT_VERBOSE = 0;
		XmlSuite suite = new XmlSuite();
		suite.setName("TestSuite");
		suite.setParallel(XmlSuite.ParallelMode.TESTS);
		suite.setVerbose(1);
		suite.setThreadCount(threads);
		testsNames.keySet().stream().forEach(testNum ->
		{
			XmlTest test = new XmlTest(suite);
			test.setName("CustomTest " + testNum);
			List<XmlClass> classes = new ArrayList<XmlClass>();
			classes.add(new XmlClass("pb.autotests.TestRunner", false));
			test.setXmlClasses(classes);
			List<String> groups = new ArrayList<String>();
			test.setIncludedGroups(groups);
			groups.addAll(getTestNamesByIndex(testsNames.get(testNum), project));
		});
		logger.debug("Generated XML {}", suite.toString());
		return suite.toXml();
	}

	public boolean writeXmlFile(String xml, String name, String project)
	{
		OutputStreamWriter fileWriter;
		try
		{
			File ff = new File(path.getXmlPathWithProject(project));
			if(!ff.exists()) ff.mkdir();
			fileWriter = new OutputStreamWriter(new FileOutputStream(ff + "/" + name), "UTF-8");
			fileWriter.write(xml);
			fileWriter.flush();
			fileWriter.close();
			return true;
		}
		catch(IOException e)
		{
			logger.error("Error!", e);
			return false;
		}
	}

	public List<String> getGroups(String project)
	{
		File groupsFile = new File(path.getProjectPath(project) + "/" + groupsFileName);
		BufferedReader bufferedReader;
		StringBuilder stringBuilder;
		try
		{
			bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(groupsFile), "UTF-8"));
			stringBuilder = new StringBuilder();
			String line = bufferedReader.readLine();
			while(line != null)
			{
				stringBuilder.append(line);
				line = bufferedReader.readLine();
			}
		}
		catch(IOException e)
		{
			logger.error("Error!", e);
			return null;
		}
		Groups groups = (Groups) SerializationMethods.toObjectFromXML(stringBuilder.toString(), Groups.class);
		if(groups != null)
		{
			return groups.getGroup();
		}
		return null;
	}

	public ArrayList<String> getTestNamesByIndex(List<String> tests, String project)
	{
		ArrayList<String> testNames;
		ArrayList<String> groups = (ArrayList<String>) getGroups(project);
		testNames = tests.stream()
				.map(index -> groups.get(Integer.parseInt(index)))
				.collect(Collectors.toCollection(ArrayList::new));
		return testNames;
	}

	public Map<Integer, List<String>> getMultipleTestMap(String[] testIndex)
	{
		Map<Integer, List<String>> multiTestMap = new TreeMap<>();
		int index = 0;
		for(String testNum : testIndex)
		{
			if(!testNum.isEmpty())
			{
				try
				{
					int testIn = Integer.valueOf(testNum);
					if(!multiTestMap.containsKey(testIn))
					{
						ArrayList<String> lstTests = new ArrayList<>();
						lstTests.add(String.valueOf(index));
						multiTestMap.put(testIn, lstTests);
					} else
					{
						multiTestMap.get(testIn).add(String.valueOf(index));
					}
					index++;
				}
				catch(NumberFormatException e)
				{
					return null;
				}
			} else
			{
				index++;
			}
		}
		logger.debug("multiTestMap {}", multiTestMap);
		return multiTestMap;
	}


	public String[] getTestsFromListMap(List<FormJson> listMap)
	{
		ArrayList<String> arrayList;
		arrayList = listMap.stream()
				.filter(formJson -> formJson.getName().equals("test"))
				.map(FormJson::getValue)
				.collect(Collectors.toCollection(ArrayList<String>::new));
		logger.debug("TestsFromListMap {}", arrayList);
		return arrayList.toArray(new String[arrayList.size()]);
	}
}
