package springfreemarker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import springfreemarker.authcore.CoreAuth;
import springfreemarker.entities.FormJson;
import springfreemarker.entities.TotpCode;
import springfreemarker.utils.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@PropertySource("classpath:config.properties")
@PropertySource(value = "file:${prop.file}", ignoreResourceNotFound = true)
@Controller
public class HomeController
{
	private static final Logger logger = LoggerFactory.getLogger(XMLGenerator.class);

	@Autowired
	ScreenUtils screenUtils;
	@Autowired
	Path path;
	@Autowired
	ScreensData screensData;
	@Autowired
	XMLGenerator xmlGenerator;
	@Autowired
	HttpUtils httpUtils;

	@Value("${token}")
	private String token;

	@RequestMapping(value = "/")
	public String loadMainPage(Model model, HttpServletRequest request)
	{
		logger.error("MainPage access IP {}", getIp(request));
		ArrayList<String> projects = path.getProjectFolderList();
		model.addAttribute("folders", projects);
		model.addAttribute("jenkinsUrl", path.getJenkinsUrl());
		logger.debug("folders {}", projects);
		logger.debug("jenkinsUrl {}", path.getJenkinsUrl());
		return "index";
	}

	@RequestMapping(value = "/{project:.+}")
	public String loadProjectPage(@PathVariable String project, Model model)
	{
		List<String> listTemplates = screenUtils.getListTemplates(project);
		List<String> screensList = screenUtils.getScreensList(project);
		model.addAttribute("listTemplates", listTemplates);
		model.addAttribute("listScreens", screensList);
		logger.debug("listTemplates {}", listTemplates);
		logger.debug("listScreens {}", screensList);
		return "project";
	}

	@RequestMapping(value = "/{project:.+}/image/{imageId:.+}", method = RequestMethod.GET)
	@ResponseBody
	public byte[] getScreensBytes(@PathVariable String project, @PathVariable String imageId)
	{
		byte[] screen = screensData.getScreenData(project, imageId);
		if(screen.length == 0)
		{
			return screensData.getPlaceholder();
		}
		return screen;
	}

	@RequestMapping(value = "/{project}/delete", method = RequestMethod.POST)
	public String delete(@PathVariable String project, HttpServletRequest httpServletRequest)
	{
		String[] screensNames = httpServletRequest.getParameterValues("screenUtils");
		if(screensNames == null)
		{
			return "redirect:/" + project;
		}
		screenUtils.deleteScreens(screensNames, project);
		return "redirect:/" + project;
	}

	@RequestMapping(value = "/{project}/make/{template:.+}", method = RequestMethod.POST)
	public String makeBaseLine(@PathVariable String project, @PathVariable String template)
	{
		screenUtils.makeImageBase(project, template);
		return "redirect:/" + project;
	}

	@RequestMapping(value = "/{project}/build/adv", method = RequestMethod.GET)
	public String run(@PathVariable String project, Model model)
	{
		List<String> groups = xmlGenerator.getGroups(project);
		if(groups == null)
		{
			model.addAttribute("error", "File \"groups.xml\" is missing in your project folder !");
			return "error";
		}
		model.addAttribute("groupsRun", xmlGenerator.getGroups(project));
		return "advanced";
	}

	@RequestMapping(value = "/{project}/build/adv", method = RequestMethod.POST)
	public String run(@PathVariable String project, HttpServletRequest httpServletRequest, Model model)
	{
		String[] tests = httpServletRequest.getParameterValues("test");
		int threads = Integer.parseInt(httpServletRequest.getParameter("threads"));
		Map<Integer, List<String>> testNamesMap = xmlGenerator.getMultipleTestMap(tests);
		if(testNamesMap == null || testNamesMap.size() == 0)
		{
			model.addAttribute("error", "U need to choose some test to run !");
			return "error";
		}
		String xml = xmlGenerator.generateMultipleTestSuite(testNamesMap, threads, project);
		String name = "testng_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".xml";
		boolean success = xmlGenerator.writeXmlFile(xml, name, project);
		if(!success)
		{
			model.addAttribute("error", "some shit happened");
			return "error";
		}
		try
		{
			String url = String.format(path.getJenkinsUrlWithProject(project) +
							"/buildWithParameters?suiteXmlFile=%s&token=%s",
					path.getXmlPath() + name, token
			);
			httpUtils.setUrl(url).sendGetRequest();
		}
		catch(IOException e)
		{
			logger.error("Error!", e);
			model.addAttribute("error", "Ann error happened while making a request to remote jenkins !");
			return "error";
		}
		return "redirect:" + path.getJenkinsUrlWithProject(project);
	}

	@RequestMapping(value = "/{project}/build/simple", method = RequestMethod.GET)
	public String runSimple(@PathVariable String project, Model model)
	{
		List<String> groups = xmlGenerator.getGroups(project);
		if(groups == null)
		{
			model.addAttribute("error", "File \"groups.xml\" is missing in your project folder !");
			return "error";
		}
		model.addAttribute("groupsRun", groups);
		return "simple";
	}

	@RequestMapping(value = "/{project}/build/simple", method = RequestMethod.POST)
	public String runSimple(@PathVariable String project, HttpServletRequest httpServletRequest, Model model)
	{
		String[] tests = httpServletRequest.getParameterValues("test");
		if(tests == null)
		{
			model.addAttribute("error", "U need to choose some test to run !");
			return "error";
		}
		ArrayList<String> testNamesByIndex = xmlGenerator.getTestNamesByIndex(Arrays.asList(tests), project);
		String xml = xmlGenerator.generateSimpleSuite(testNamesByIndex);
		String name = "testng_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".xml";
		xmlGenerator.writeXmlFile(xml, name, project);
		try
		{
			String url = String.format(path.getJenkinsUrlWithProject(project) +
							"/buildWithParameters?suiteXmlFile=%s&token=%s",
					path.getXmlPath() + name, token
			);
			httpUtils.setUrl(url).sendGetRequest();
		}
		catch(IOException e)
		{
			logger.error("Error!", e);
			model.addAttribute("error", "Ann error happened while making a request to remote jenkins !");
			return "error";
		}
		return "redirect:" + path.getJenkinsUrlWithProject(project);
	}

	@RequestMapping(value = "/{project}/build/error", method = RequestMethod.GET)
	public String error(Model model)
	{
		model.addAttribute("error", "Not Found !");
		return "error";
	}

	@RequestMapping(value = "/{project}/build/preview/adv", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String preview(@RequestBody String body, @PathVariable String project)
	{
		List<FormJson> formJson = SerializationMethods.toObjectFromJsonList(body, FormJson.class);
		if(formJson == null || formJson.size() == 0)
		{
			return "U need to choose some test to run !";
		}
		int threads = Integer.parseInt(formJson.get(0).getValue());
		String[] tests = xmlGenerator.getTestsFromListMap(formJson);
		Map<Integer, List<String>> testNamesMap = xmlGenerator.getMultipleTestMap(tests);
		if(testNamesMap == null || testNamesMap.size() == 0)
		{
			return "U need to choose some test to run !";
		}
		return xmlGenerator.generateMultipleTestSuite(testNamesMap, threads, project);
	}

	@RequestMapping(value = "/{project}/build/preview/simple", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String previewSimple(@RequestBody String body, @PathVariable String project)
	{
		List<FormJson> formJson = SerializationMethods.toObjectFromJsonList(body, FormJson.class);
		if(formJson == null || formJson.size() == 0)
		{
			return "U need to choose some test to run !";
		}
		String[] tests = xmlGenerator.getTestsFromListMap(formJson);
		ArrayList<String> testNamesByIndex = xmlGenerator.getTestNamesByIndex(Arrays.asList(tests), project);
		return xmlGenerator.generateSimpleSuite(testNamesByIndex);
	}

	@RequestMapping(value = "/auth", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	@ResponseBody
	public TotpCode getTotpCode(@RequestParam(value="code", required=true) String code, HttpServletRequest request)
	{
		logger.error("OTP access IP {}", getIp(request));
		String otp = CoreAuth.computePin(code);
		return new TotpCode(otp);
	}

	public String getIp(HttpServletRequest request)
	{
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		return ipAddress;
	}
}
